FROM orleon/lamp:latest

COPY . /app/
COPY bin/run.sh /
RUN chmod +x /run.sh
WORKDIR /app
COPY bin/supervisord-php.conf /etc/supervisor/conf.d/
COPY bin/supervisord.conf /etc/supervisor/
RUN rm -rf /etc/supervisor/conf.d/supervisord-apache2.conf

RUN sed -i 's/max_file_uploads.*/max_file_uploads = 1000/' /etc/php/7.4/cli/php.ini && sed -i 's/max_file_uploads.*/max_file_uploads = 1000/' /etc/php/7.4/apache2/php.ini && sed -i 's/upload_max_filesize.*/upload_max_filesize = 1000M/' /etc/php/7.4/cli/php.ini && sed -i 's/upload_max_filesize.*/upload_max_filesize = 1000M/' /etc/php/7.4/apache2/php.ini && sed -i 's/post_max_size.*/post_max_size = 1000M/' /etc/php/7.4/cli/php.ini && sed -i 's/post_max_size.*/post_max_size = 1000M/' /etc/php/7.4/apache2/php.ini

EXPOSE 80 8080 3306
CMD ["/run.sh"]
