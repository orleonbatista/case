## Getting Started

Clone this project using the following commands:

```
git clone https://gitlab.com/orleonbatista/case.git
cd case
```

## Building or getting a Docker Image

To build an image, from the project directory, run:
```
docker build -t orleon/case .
```
To get an image already built, from the project directory, run:

```
docker pull orleon/case:latest
```

### Run the application

From the project directory, run:

```
docker run -d -p 8080:8080 orleon/case
```

Then open `http://localhost:8080` and you will see the application.

## NOTE
Panel Okta
https://dev-09713860-admin.okta.com/

